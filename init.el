;; === SETUP ===
;;(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(require 'package) ;; You might already have this line
(setq package-enable-at-startup nil)
;;(setq package-check-signature "allow-unsigned")
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
;;(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;;(package-initialize)
;(package-refresh-contents)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
;; === CUSTOM CHECK FUNCTION ===
;; (defun ensure-package-installed (&rest packages)
;;   "Assure every package is installed, ask for installation if it’s not.
;;    Return a list of installed packages or nil for every skipped package."
;;   (mapcar
;;    (lambda (package)
;;      (unless (package-installed-p package)
;;        (package-install package)))
;;      packages)
;;   )

;(ensure-package-installed 'use-package)

(require 'use-package)
(setq use-package-always-ensure t)
(org-babel-load-file "~/.emacs.d/configuration.org")
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(ansi-color-faces-vector
;;    [default default default italic underline success warning error])
;;  '(ansi-color-names-vector
;;    ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
;;  '(custom-enabled-themes nil)
;;  '(package-selected-packages
;;    (quote
;;     (elfeed switch-window async el-get org popup-kill-ring sudo-edit avy swiper which-key dmenu exwm use-package-ensure-system-package linum-relative company-emacs-eclim auto-complete))))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(conda-anaconda-home "/home/niels/anaconda3/")
 '(dired-listing-switches "-l")
 '(org-agenda-files
   '("~/Dokumente/Studium/Informatik/BachelorArbeit/Sources/Latex/Thesis/Bachelor.org"))
 '(package-selected-packages
   '(org-ref org-drill gnu-elpa-keyring-update dockerfile-mode yasnippet which-key use-package switch-window swiper sudo-edit projectile popup-kill-ring magit lsp-ui lsp-java elfeed dmenu dap-mode company-lsp beacon auto-complete)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-ui-sideline-code-action ((t (:foreground "red"))))
 '(region ((t (:extend t :background "pale green" :distant-foreground "gtk_selection_fg_color")))))
